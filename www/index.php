<html>
    <head>
        <title>starypc</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="style.css" />
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    </head>
    <body>
        <a href="index.php?p=main.html"><h1>starypc</h1></a>
        <p>strona domowa staregopc</p>
        <hr />
        Nawigacja:
        <a href="index.php?p=main.html">Strona główna</a>
        <a href="index.php?p=status.php">Status</a>
        <a href="index.php?p=kontakt.html">Kontakt</a>
        <a href="index.php?p=minecraft/main.html">Minecraft</a>
        <br />
        Strony osobiste:
        <a href="/~grzesiek11" target="_blank">Grzesiek11</a>
        <a href="/~tadeln" target="_blank">TadeLn</a>
        <br />
        Usługi:
        <a href="/shell" target="_blank">Shell</a>
        <a href="/nextcloud" target="_blank">Nextcloud</a>
        <br />
        Skróty:
        <a href="index.php?p=minecraft/lody.html">Lody</a>
        <a href="pages/minecraft/map" target="_blank">Mapa 3D (MC)</a>
        <a href="pages/minecraft/stats" target="_blank">Statystyki (MC)</a>
        <hr />
        <?php
        if (isset($_GET["p"])) {
            if ($_GET["p"] == "") {
                $page = "main.html";
            } else {
                $page = $_GET["p"];
            }
        } else {
            $page = "main.html";
        }
        $dirPath = realpath(dirname("pages/$page"));
        $base = getcwd()."/pages";
        if (substr($dirPath, 0, strlen($base)) == $base) {
            if (file_exists("pages/$page")) {
                include("pages/$page");
            } else {
                include("pages/error/404.html");
            }
        } else {
            include("pages/error/401.html");
        }
        ?>
        <hr />
        <small>
            Grzesiek11 & TadeLn 2018-2019
            <br />
            Grzesiek11 2020
            <br />
            <a href="https://gitlab.com/grzesiek11/starypc" target="_blank">Licensed under MIT.</a>
            <br />
            <a href="index.php?p=credits.html">Credits&Copyrights</a>
        </small>
    </body>
</html>