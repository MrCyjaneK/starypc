#!/bin/sh
# checkspeed.sh - Check internet speed to a file.

speedtest='~/.local/bin/speedtest'
directory='~/speedtests'

"$speedtest" --json > "$directory/$(date +%d-%m-%Y_%H).json"
