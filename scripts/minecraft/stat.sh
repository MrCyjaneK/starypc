#!/bin/sh
# stat.sh - mcstats wrapper script.

# TODO
# - config
# - more features

name="Lody Vanillowe"
inactive=99999
webdir="/var/www/html/pages/minecraft/stats"
mcdir="/srv/minecraft/survival"

cd "$webdir"
python3 update.py --server "$mcdir" --server-name "$name" --inactive-days $inactive
