#!/bin/bash
# overviewer.sh - Overviewer for both POI (fast) and map (slow), depending on given argument.

# TODO
# - POSIX rewrite
# - proper option checks

overviewercmd="python3 $HOME/programy/overviewer/overviewer.py"
rendercfg="/srv/minecraft/overviewer/overviewer.conf"
poicfg="/srv/minecraft/overviewer/poi.conf"

if [ -z $1 ]; then
    exit
elif [ $1 == "poi" ]; then
    cfg="$poicfg"
    flags=("--genpoi")
elif [ $1 == "render" ]; then
    cfg="$rendercfg"
else
    exit
fi

$overviewercmd ${flags[@]} -c "$cfg"
