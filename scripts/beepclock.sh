#!/bin/sh
# beepclock.sh - useless script for beeping out hours.

# Set to crontab for every hour and enjoy ;)

hour=$(date +"%H")

if [ $hour -gt 12 ]; then
    hour=$((hour-12))
elif [ $hour -eq 0 ]; then
    hour=12
    exit
fi

i=0
while [ $i -lt $hour ]; do
    beep
    sleep 0.5
    i=$((i+1))
done
